import asyncio
import aiorfc
import time


NUM_MSG = 10000


async def test(connection):
    client = await connection.create_client("test")
    start = time.time()
    tasks = []
    for i in range(NUM_MSG):
        tasks.append(loop.create_task(client.test("a", "b")))
    for task in tasks:
        await task
    print("calls:", (NUM_MSG / (time.time() - start)), "qps")


loop = asyncio.get_event_loop()

with aiorfc.RFCConnection(loop, prefetch_count=1000) as connection:
    loop.run_until_complete(test(connection))
