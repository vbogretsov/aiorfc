import asyncio
import logging.config

import aiorfc


LOG_CONF = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "class": "logging.Formatter",
            "format": "%(asctime)s %(name)s [%(levelname)s]: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "standard",
            "level": "DEBUG"
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "standard",
            "level": "DEBUG",
            "filename": "server.log",
            "maxBytes": 1048576,
            "backupCount": 5
        }
    },
    "loggers": {
        "": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
            "propagate": True
        }
    }
}


async def test(a, b):
    return "".join((a, b))


async def start(connection):
    await connection.run_server("test", (test,))


logging.config.dictConfig(LOG_CONF)
loop = asyncio.get_event_loop()

with aiorfc.RFCConnection(loop, prefetch_count=1000) as connection:
    loop.run_until_complete(start(connection))
    loop.run_forever()
