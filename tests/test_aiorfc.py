# -*- coding: utf-8 -*-
"""Tests cases for the module aiorfc

To run tests AMQP broker should be up and running on the localhost and the
defualt port.
"""
import asyncio
import time
from unittest import mock

import aioamqp
import aiorfc
import pytest


TIME_CONSUMING_TASK_DELAY = 2


class CustomError(Exception):
    def __init__(self, msg, errors):
        super().__init__(msg)
        self.errors = errors


def onerror(ex):
    if isinstance(ex, CustomError):
        return True, {"errors": ex.errors}
    return False, None


async def add(a, b):
    return {"a": a, "b": b, "sum": a + b}


async def fail(a):
    raise CustomError("test-error", {
        "email": "invalid email",
        "password": "ivalid password",
        "data": a
    })


async def unhandled_error():
    raise ValueError("unhandled error")


def sync(a, b):
    return a + b


async def time_consuming():
    await asyncio.sleep(TIME_CONSUMING_TASK_DELAY)


@pytest.fixture
def rfc_connection(request, aioamqpmock, event_loop):
    connection = aiorfc.RFCConnection(event_loop)
    event_loop.run_until_complete(connection.open())

    def fin():
        event_loop.run_until_complete(connection.close())

    request.addfinalizer(fin)
    return connection


@pytest.fixture
def server(event_loop, rfc_connection):
    name = "aiorfc-test"
    handlers = (add, fail, time_consuming, sync, unhandled_error)
    event_loop.run_until_complete(
        rfc_connection.run_server(name, handlers, onerror))
    return name


@pytest.fixture
def client(event_loop, rfc_connection, server):
    return event_loop.run_until_complete(
        rfc_connection.create_client(server, timeout=1))


def test_rfc_connection_with_statement(event_loop):
    with aiorfc.RFCConnection(event_loop):
        pass


def test_rfc_connection_with_statement_enters_restore_mode(event_loop, mocker):
    scope = {"restored": False}

    async def open_failed(this):
        raise aioamqp.AmqpClosedConnection()

    async def close_mock():
        pass

    async def restore_mock(this, ex):
        this.transport = mock.Mock(close=lambda: None)
        this.protocol = mock.Mock(close=close_mock)
        this.channel = mock.Mock(close=close_mock)
        scope["restored"] = True

    mocker.patch("aiorfc.RFCConnection.open", new=open_failed)
    mocker.patch("aiorfc.RFCConnection.restore", new=restore_mock)

    with aiorfc.RFCConnection(event_loop):
        pass

    assert scope["restored"]


@pytest.mark.asyncio
async def test_remote_call_returns_correct_result(client):
    res = await client.add(1, 2)
    assert res == {"a": 1, "b": 2, "sum": 3}


@pytest.mark.asyncio
async def test_n_calls_completed(event_loop, client):
    tasks = {}
    for i in range(100):
        tasks[(1, i)] = event_loop.create_task(client.add(1, i))
    for task in tasks:
        res = await tasks[task]
        assert res == {"a": 1, "b": task[1], "sum": 1 + task[1]}


@pytest.mark.asyncio
async def test_client_raises_call_failed_if_server_raised_it(client):
    try:
        await client.fail(2)
        assert False
    except aiorfc.RFCCallFailed as ex:
        assert ex.ex == {
            "errors": {
                "data": 2,
                "email": "invalid email",
                "password": "ivalid password"
            }
        }


@pytest.mark.asyncio
async def test_client_raises_unhandled_error_if_server_raised_it(client):
    try:
        await client.unhandled_error()
        assert False
    except aiorfc.RFCUnhandledError as ex:
        assert str(ex) == "unhandled error"


@pytest.mark.asyncio
async def test_client_raises_if_unknown_function_name(client):
    with pytest.raises(aiorfc.RFCUknownFunction):
        await client.unknown_function()


@pytest.mark.asyncio
async def test_time_consuming_call_raises_timeout(client):
    with pytest.raises(asyncio.TimeoutError):
        await client.time_consuming()


@pytest.mark.asyncio
async def test_sync_handler_returns_correct_result(client):
    res = await client.sync(3, 4)
    assert res == 7


@pytest.mark.asyncio
async def test_client_can_not_wait_for_result(client):
    start = time.time()
    resp = await client.time_consuming(wait_response=False)
    assert resp is None
    assert time.time() - start < TIME_CONSUMING_TASK_DELAY
