# aiorfc 0.2.0

## Installation

````
pip install -e git+https://gitlab.com/vbogretsov/aiorfc.git#egg=aiorfc-0.2.0
````

## Usage

Assuming you have up and running RabbitMQ server with default settings.

````python
#  server.py
import asyncio
import aiorfc


async def add(a, b):
    return a + b

async def time_consuming_task():
    await asyncio.sleep(1000)


loop = asyncio.get_event_loop()
with aiorfc.RFCConnection(loop) as connection:
    loop.run_until_complete(connection.run_server("test", (sum, )))
    loop.run_forever()
````

````python
#  client.py
import asyncio
import aiorfc


async def call(connection):
    client = await connection.create_client("test")
    result = await client.sum(1, 2)
    await client.time_consuming_task(wait_response=False)



loop = asyncio.get_event_loop()
with aiorfc.RFCConnection(loop) as connection:
    loop.run_until_complete(start(connection))
````

## API

### RFCConnection

`RFCConnection.__init__(self, loop=None, **kwargs):`

__Parameters__:
  * __loop__ -- event loop, if `None` default one will be used.
  * __restore_timeout_seconds__ -- interval between restore attempts if AMQP conneciton was lost.
  * __channel_max__ (int) –- specifies highest channel number that the server permits. Usable channel numbers are in the range 1..channel-max. Zero indicates no specified limit.
  * __frame_max__ (int) –- the largest frame size that the server proposes for the connection, including frame header and end-byte. The client can negotiate a lower value. Zero means that the server does not impose any specific limit but may reject very large frames if it cannot allocate resources for them.
  * __heartbeat__ (int) –- the delay, in seconds, of the connection heartbeat that the server wants. Zero means the server does not want a heartbeat.
  * client_properties (dict) –- configure the client to connect to the AMQP server.
  * __prefetch_count__ (int) -- specifies a prefetch window in terms of whole messages.

`RFCConnection.run_server(self, name, functions, onerror=str):`

Start consuming requests to the functions provided.

__Parameters__:
 * __name__ (str) -- server name.
 * __functions__ (iterable) -- request handlers.
 * __onerror__ (callable) -- converts exception to dict.

`RFCConnection.create_client(self, srvname):`

Create client for the server with the name provided.

__Parameters__:
* __srvname__ (str) -- server name.

## License

See the LICENSE file.