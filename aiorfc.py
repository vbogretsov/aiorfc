# -*- coding: utf-8 -*-
"""Aynchronous AMQP based remote function call library.
"""
import asyncio
import inspect
import logging
import pickle
import uuid

import aioamqp

ERFC_UKNOWN_FUNCTION = 1
ERFC_UNHANDLED_ERROR = 2
ERFC_CALL_FAILED = 3

LOG = logging.getLogger(__name__)


class RFCError(Exception):
    """Base RFC error.
    """
    pass


class RFCUknownFunction(RFCError):
    """Occurs when remote function was not found on the server.
    """
    pass


class RFCUnhandledError(RFCError):
    """Occurs when a remote funciton call was failed but error was not handled.
    """
    pass


class RFCCallFailed(RFCError):
    """Occurs when a remote function call was failed.

    Attributes:
        ex (Exception): original exception.
    """

    def __init__(self, msg, ex):
        super().__init__(msg)
        self.ex = ex


def _erfc_unknown_function(response):
    raise RFCUknownFunction("unknown function name")


def _erfc_unhandled_error(response):
    raise RFCUnhandledError(response["data"])


def _erfc_call_failed(response):
    raise RFCCallFailed("remote function call failed", response["data"])


def _df_onerror(ex):
    return False, None


ERROR_HANDLERS = {
    ERFC_UKNOWN_FUNCTION: _erfc_unknown_function,
    ERFC_UNHANDLED_ERROR: _erfc_unhandled_error,
    ERFC_CALL_FAILED: _erfc_call_failed
}

IQUEUE = "i{0}"
OQUEUE = "o{0}{1}"


def pack(data):
    """Serialize data.

    Args:
        data (object): Data to serizlize.
    """
    return pickle.dumps(data)


def unpack(data):
    """Deserialize data.

    Args:
        data (object): Data to deserizlize.
    """
    return pickle.loads(data, encoding="utf-8")


class Request:
    """RFC request.

    Attributes:
        id (str): Request id -- random unique string.
        waiter (asyncio.Event): event object being signaled when a response
            is got.
        response (dict): Response data.
    """

    def __init__(self):
        self.id = str(uuid.uuid4())
        self.waiter = asyncio.Event()
        self.response = None


class RFCConnection:
    """AMQP connection.
    """

    def __init__(self,
                 loop,
                 restore_timeout_seconds=5,
                 prefetch_count=0,
                 **kwargs):
        self.loop = loop or asyncio.get_event_loop()
        self.restore_timeout_seconds = restore_timeout_seconds
        self.prefetch_count = prefetch_count
        self.kwargs = kwargs
        self.transport = None
        self.protocol = None
        self.channel = None
        self.closing = False
        # restore_attempt is a workaround for the issue
        # https://github.com/Polyconseil/aioamqp/issues/124
        self.restore_attempt = 0
        self.servers = {}
        self.clients = {}

    def __enter__(self):
        try:
            self.loop.run_until_complete(self.open())
        except (aioamqp.AmqpClosedConnection, OSError) as ex:
            self.loop.run_until_complete(self.restore(ex))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.loop.run_until_complete(self.close())

    async def open(self):
        """Open connection.
        """
        LOG.info("opening AMQP connection ....")
        self.transport, self.protocol = await aioamqp.connect(
            on_error=self.restore, **self.kwargs)
        self.channel = await self.protocol.channel()
        await self.channel.basic_qos(
            prefetch_count=self.prefetch_count, connection_global=False)
        LOG.info("opening AMQP connection done")

    async def close(self):
        """Close conneciton.
        """
        self.closing = True
        for client in self.clients.values():
            await self.channel.queue_delete(client.oqueue)
        await self.channel.close()
        await self.protocol.close()
        self.transport.close()

    async def restore(self, exception):
        """Restore the lost conneciton.
        """
        if self.closing:
            return
        if self.restore_attempt == 1:
            return

        self.restore_attempt = 1
        try:
            LOG.info("restoring lost connection ....")
            await self.open()
            LOG.debug("servers: %s", self.servers)
            LOG.debug("clients: %s", self.clients)
            for server in self.servers.values():
                await self.run_server(server.name, server.functions)
            for client in self.clients:
                await self.create_client(client)
            LOG.info("restoring lost connection done")
        except (aioamqp.AmqpClosedConnection, OSError):
            self.restore_attempt = 0
            await asyncio.sleep(self.restore_timeout_seconds)
            await self.restore(exception)
        finally:
            self.restore_attempt = 0

    async def run_server(self, srvname, functions, onerror=_df_onerror):
        """Start consuming requests to the functions provided.

        Args:
            srvname (str): Server name.
            functions (iterable): Request handlers.
            onerror (callable): converts exception to dict.
        """
        server = self.servers.get(srvname, None)
        if server is None:
            server = Server(self, srvname, functions, onerror)
            self.servers[srvname] = server

        await self.channel.queue_declare(
            queue_name=server.iqueue, durable=True)

        LOG.info("starting server '%s' ....", srvname)
        await self.channel.basic_consume(
            server.on_request, queue_name=server.iqueue)
        LOG.info("starting server '%s' done", srvname)

    async def create_client(self, srvname, timeout=60):
        """Create RFC client.

        Args:
            srvname (str): Server name.
            timeout (int, float): Remote call timeout.

        Returns:
            configured client.
        """
        client = self.clients.get(srvname, None)
        if client is None:
            client = Client(self, srvname, timeout)
            self.clients[srvname] = client

        await self.channel.queue_declare(queue_name=client.oqueue)
        await self.channel.basic_consume(
            client.on_response, no_ack=True, queue_name=client.oqueue)
        return client


class Server:
    """RFC server.

    Attributes:
        connection: AMQP connection.
        name (str): server name.
        functions (iterable): request handlers.
    """

    def __init__(self, connection, name, functions, onerror):
        self.connection = connection
        self.name = name
        self.iqueue = IQUEUE.format(name)
        if isinstance(functions, dict):
            self.functions = functions
        else:
            self.functions = {fn.__name__: fn for fn in functions}
        self.onerror = onerror

    async def _invoke(self, function, request):
        if function:
            try:
                if inspect.iscoroutinefunction(function):
                    result = await function(*request["ar"], **request["kw"])
                else:
                    result = function(*request["ar"], **request["kw"])
                response = {"code": 0, "data": result}
            except Exception as ex:
                error_handled, error_data = self.onerror(ex)
                if error_handled:
                    response = {"code": ERFC_CALL_FAILED, "data": error_data}
                else:
                    LOG.exception(ex)
                    response = {"code": ERFC_UNHANDLED_ERROR, "data": str(ex)}
        else:
            response = {"code": ERFC_UKNOWN_FUNCTION}
        return response

    async def _call_and_response(self, function, request, properties):
        LOG.debug("[%s] recv [%s]: %s", self.name, properties.correlation_id,
                  request)
        response = await self._invoke(function, request)
        LOG.debug("[%s] send [%s]: %s", self.name, properties.correlation_id,
                  response)
        await self.connection.channel.basic_publish(
            payload=pack(response),
            exchange_name="",
            routing_key=properties.reply_to,
            properties={"correlation_id": properties.correlation_id})

    async def _call_not_response(self, function, request):
        LOG.debug("[%s] recv [not-wait]: %s", self.name, request)
        await self._invoke(function, request)

    async def _call(self, channel, body, envelope, properties):
        request = unpack(body)
        function = self.functions.get(request["fn"])

        if properties.reply_to:
            await self._call_and_response(function, request, properties)
        else:
            await self._call_not_response(function, request)

        await self.connection.channel.basic_client_ack(
            delivery_tag=envelope.delivery_tag)

    async def on_request(self, channel, body, envelope, properties):
        """RFC server response handler.

        Args:
            channel: AMQP channel.
            body: request body.
            envelope: request envelope.
            properties: request metadata.
        """
        loop = asyncio.get_event_loop()
        loop.create_task(self._call(channel, body, envelope, properties))


class Client:
    """RFC client.

    Attributes:
        conneciton: RFC connection.
        srvname (str): Remote server name.
        timeout (int, float): Request timeout in seconds.
        iqueue (str): Input queue name where to send requests.
        oqueue (str): Output queue name where to send requests.
        requests (dict): Collection of incompleted requests.
    """

    def __init__(self, connection, srvname, timeout):
        self.connection = connection
        self.srvname = srvname
        self.timeout = timeout
        self.iqueue = IQUEUE.format(srvname)
        self.oqueue = OQUEUE.format(srvname, uuid.uuid4())
        self.requests = {}

    async def _call_and_wait(self, name, *args, **kwargs):
        request = Request()
        self.requests[request.id] = request

        LOG.debug("RFC request [%s]: '%s.%s(%s, %s)' ", request.id,
                  self.srvname, name, args, kwargs)

        await self.connection.channel.basic_publish(
            payload=pack({
                "fn": name,
                "ar": args,
                "kw": kwargs
            }),
            exchange_name="",
            routing_key=self.iqueue,
            properties={"correlation_id": request.id,
                        "reply_to": self.oqueue})
        await asyncio.wait_for(request.waiter.wait(), timeout=self.timeout)

        LOG.debug("RFC response [%s]: %s", request.id, request.response)

        del self.requests[request.id]
        if request.response["code"] == 0:
            return request.response["data"]
        return ERROR_HANDLERS[request.response["code"]](request.response)

    async def _call_not_wait(self, name, *args, **kwargs):
        LOG.debug("RFC request [not-wait]: '%s.%s(%s, %s)' ", self.srvname,
                  name, args, kwargs)
        await self.connection.channel.basic_publish(
            payload=pack({
                "fn": name,
                "ar": args,
                "kw": kwargs
            }),
            exchange_name="",
            routing_key=self.iqueue,
            properties={"reply_to": None})

    def __getattr__(self, name):

        async def call(*args, wait_response=True, **kwargs):
            if wait_response:
                return await self._call_and_wait(name, *args, **kwargs)
            await self._call_not_wait(name, *args, **kwargs)

        return call

    async def on_response(self, channel, body, envelope, properties):
        """RFC Client response handler.

        Args:
            channel: AMQP channel.
            body: response bosy.
            envelope: request envelope.
            properties: request metadata properties.
        """
        LOG.debug("received response: %s", properties.correlation_id)
        request = self.requests.get(properties.correlation_id)
        if request:
            request.response = unpack(body)
            request.waiter.set()
