# -*- coding:utf-8 -*-
"""A setuptools based setup module for 'yamlconf'.
"""
import setuptools

DESCRIPTTION = """
Library for asynchronous remote function calls based on AMQP protocol.
"""

GITLAB_BASE = "git+https://gitlab.com/vbogretsov/{0}"

setuptools.setup(
    name="aiorfc",
    version="0.2.0",
    description=DESCRIPTTION,
    url="https://vbogretsov@gitlab.com/vbogretsov/aiorfc.git",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    py_modules=["aiorfc"],
    setup_requires=["pytest-runner"],
    tests_require=[
        "pytest", "pytest-cov", "pytest-asyncio", "pytest-mock",
        "pytest-aioamqp"
    ],
    install_requires=["aioamqp"],
    dependency_links=[
        GITLAB_BASE.format("pytest-aioamqp.git#egg=pytest-aioamqp-0.1.0")
    ])
